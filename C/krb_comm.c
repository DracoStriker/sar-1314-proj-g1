
/*
 * Métodos auxiliares comuns tanto ao servidor como ao cliente
 * 
 * 
 */


#include "krb_comm.h"

static void PrintBuffer(const char *inBuffer,
        size_t inLength) {
    int i;

    for (i = 0; i < inLength; i += 16) {
        int l;
        for (l = i; l < (i + 16); l++) {
            if (l >= inLength) {
                printf("  ");
            } else {
                u_int8_t *byte = (u_int8_t *) inBuffer + l;
                printf("%2.2x", *byte);
            }
            if ((l % 4) == 3) {
                printf(" ");
            }
        }
        printf("   ");
        for (l = i; l < (i + 16) && l < inLength; l++) {
            printf("%c", ((inBuffer[l] > 0x1f) &&
                    (inBuffer[l] < 0x7f)) ? inBuffer[l] : '.');
        }
        printf("\n");
    }
    printf("\n");
}


/* --------------------------------------------------------------------------- */

/* Standard network read loop, accounting for EINTR, EOF and incomplete reads  */

static int ReadBuffer(int inSocket,
        size_t inBufferLength,
        char *ioBuffer) {
    int error = 0;
    /* sszie_t: Represent the sizes of blocks that can be read or written in a single operation*/
    ssize_t totalReadBytes;
    char *ptr;

    if (ioBuffer == NULL) {
        error = EINVAL;
    }

    if (!error) {
        ptr = ioBuffer;
        totalReadBytes = 0;
        do {
            ssize_t readBytes = read(inSocket, ptr, inBufferLength - totalReadBytes);
            if (readBytes < 0) {
                /* Try again on EINTR */
                if (errno != EINTR) {
                    error = errno;
                }
            } else if (readBytes == 0) {
                //set error to force cycle break
                error = ECONNRESET;
            } else {
                //add read bytes to buffer and move pointer
                ptr += readBytes;
                totalReadBytes += readBytes;
            }
        } while (!error && (totalReadBytes < inBufferLength));
    }

    if (error) {
        printError(error, "ReadBuffer failed");
    }

    return error;
}

/* --------------------------------------------------------------------------- */

/* Standard network write loop, accounting for EINTR and incomplete writes     */

static int WriteBuffer(int inSocket,
        const char *inBuffer,
        size_t inBufferLength) {
    int error = 0;
    ssize_t totalBytes; //total bytes written
    ssize_t last; //bytes written last round
    const char *ptr;
    if (inBufferLength <= 0 || inBuffer == NULL) {
        error = EINVAL;
    }

    if (!error) {
        ptr = inBuffer;
        totalBytes = 0;

        do {

            last = write(inSocket, ptr, inBufferLength - totalBytes);

            if (last < 0) {
                /* Try again on EINTR */
                if (errno != EINTR) {
                    error = errno;
                }
            } else {
                ptr += last;
                totalBytes += last;
            }
        } while (!error && (totalBytes < inBufferLength));
    }

    if (error) {
        printError(error, "WriteBuffer failed");
    }

    return error;

}

/* --------------------------------------------------------------------------- */

/* Read a GSS token (length + data) off the network                            */

int ReadToken(int inSocket,
        gss_buffer_desc *token) {
    int error = 0;
    char *tokenValue = NULL;
    u_int32_t tokenLength = 0;
    if(DEBUG){printf("Going to read...");}
    //ler os primeiros 4 bytes. Devolvem o tamanho do token.

    if (!error) {
        error = ReadBuffer(inSocket, sizeof (int), (char *) &tokenLength);
    }
    if (!error) {
        tokenLength = ntohl(tokenLength);
        //reservar o espaço necessário para ler a mensagem
        tokenValue = malloc(tokenLength);
        memset(tokenValue, 0, tokenLength);
        //ler o token.
        error = ReadBuffer(inSocket, tokenLength, tokenValue);
    }

    if (!error) {
        if(DEBUG){printf("Read token:\n");}
        token->length = tokenLength;
        token->value = tokenValue;
        if(DEBUG){PrintBuffer(tokenValue, tokenLength);}
        tokenValue = NULL; /* only free on error */
    } else {
        printError(error, "ReadToken failed");
    }

    if (tokenValue) {
        free(tokenValue);
    }

    return error;
}

/* --------------------------------------------------------------------------- */

/* Write a GSS token (length + data) onto the network                          */

int WriteToken(int inSocket,
        gss_buffer_desc token) {
    int error = 0;
    int len;
    u_int32_t tokenLength = htonl(token.length);
    if(DEBUG){printf("Going to write..");}
    if (token.value == NULL) {
        error = EINVAL;
    }

    //escrever os primeiros 4 bytes com o tamanho do token.
    if (!error) {
        error = WriteBuffer(inSocket, (char *) &tokenLength, sizeof (int));
    }
    //escrever o token    
    if (!error) {
        error = WriteBuffer(inSocket, (const char *) token.value, (int) token.length);
    }

    if (!error) {
        if(DEBUG){printf("Wrote token:\n");
        PrintBuffer(token.value, token.length);}
    } else {
        printError(error, "WriteToken failed");
    }

    return error;
}

/* --------------------------------------------------------------------------- */

/* Read an encrypted GSS token (length + encrypted data) off the network       */


int ReadEncryptedToken(int inSocket,
        const gss_ctx_id_t inContext,
        gss_buffer_t token) {
    int error = 0;
    OM_uint32 majorStatus;
    OM_uint32 minorStatus = 0;
    gss_buffer_desc outputToken;
    char *unencryptedToken = NULL;

    //consistencia
    if (!inContext) {
        error = EINVAL;
    }
    if (!token) {
        error = EINVAL;
    }

    gss_buffer_desc inputToken = {0,NULL};
    if (!error) {
        error = ReadToken(inSocket, &inputToken);
    }
    
    if (!error) {
        int encrypted = 0; /* did mechanism encrypt/integrity protect? */

        majorStatus = gss_unwrap(&minorStatus,
                inContext,
                &inputToken,
                &outputToken,
                &encrypted,
                NULL /* qop_state */);
        if (majorStatus != GSS_S_COMPLETE) {
            if(DEBUG){display_status("gss_unwrap", majorStatus, minorStatus);}
            error = minorStatus ? minorStatus : majorStatus;
        } else if (!encrypted) {
            if(DEBUG){fprintf(stderr, "WARNING!  Mechanism not using encryption!");}
            error = 1; //Check this error!! pls
        }
    }
    if (!error) {
        unencryptedToken = malloc(outputToken.length);
        if (unencryptedToken == NULL) {
            error = ENOMEM;
        }
    }

    if (!error) {
        memcpy(unencryptedToken, outputToken.value, outputToken.length);
        if(DEBUG){printf("Unencrypted token:\n");
        PrintBuffer(unencryptedToken, outputToken.length);}
        token->length = outputToken.length;
        token->value = unencryptedToken;
        unencryptedToken = NULL; /* only free on error */
        if (outputToken.value) {
        gss_release_buffer(&minorStatus, &outputToken);
    }

    if (inputToken.value) {
        gss_release_buffer(&minorStatus, &inputToken);
    }
    if (unencryptedToken) {
        free(unencryptedToken);
    }

    } else {
        if(DEBUG){printError(error, "ReadEncryptedToken failed");}
    }

    
    return error;
}


/* --------------------------------------------------------------------------- */

/* Write an encrypted GSS token (length + encrypted data) onto the network     */
int WriteEncryptedToken(int inSocket,
        const gss_ctx_id_t inContext,
        gss_buffer_t token) {
    int error = 0;
    OM_uint32 majorStatus;
    OM_uint32 minorStatus = 0;
    gss_buffer_desc outputBuffer = {0, NULL};

    //consistencia
    if (!inContext) {
        error = EINVAL;
    }
    if (!token) {
        error = EINVAL;
    }
    if (!error) {
        gss_buffer_desc inputBuffer = {token->length, (char *) token->value};
        int encrypt = 1; /* do encryption and integrity protection */
        int encrypted = 0; /* did mechanism encrypt/integrity protect? */

        majorStatus = gss_wrap(&minorStatus,
                inContext,
                encrypt,
                GSS_C_QOP_DEFAULT,
                &inputBuffer,
                &encrypted,
                &outputBuffer);
        if (majorStatus != GSS_S_COMPLETE) {
            if(DEBUG){display_status("gss_wrap", majorStatus, minorStatus);}
            error = minorStatus ? minorStatus : majorStatus;
        } else if (!encrypted) {
            if(DEBUG){fprintf(stderr, "WARNING!  Mechanism does not support encryption!");}
            error = -1; //Check this error!! pls
        }
    }

    if (!error) {
        if(DEBUG){printf("Unencrypted token:\n");
        PrintBuffer(token->value, token->length);}
        error = WriteToken(inSocket, outputBuffer);
    }

    if (error) {
        if(DEBUG){printError(error, "WriteToken failed");}
    }
    if (outputBuffer.value) {
        gss_release_buffer(&minorStatus, &outputBuffer);
    }

    return error;
}

/* --------------------------------------------------------------------------- */

/* Print error                                                             */

void printError(int inError,
        const char *inString) {
    if(inError == -1765328164)
    {
        //Unknown error to the mechanism. Must manually print it.
        printf("Cannot resolve network address for KDC in requested realm\n");
        return;
    }
    errno = inError;
    perror(inString);
}



/* --------------------------------------------------------------------------- */

/* Print GSSAPI errors                                                         */

static void display_status_aux(char* routine, OM_uint32 code, int type) {
    OM_uint32 maj_stat, min_stat;
    gss_buffer_desc msg = GSS_C_EMPTY_BUFFER;
    OM_uint32 msg_ctx;

    msg_ctx = 0;
    while (1) {
        maj_stat = gss_display_status(&min_stat, code,
                type, g_mechOid,
                &msg_ctx, &msg);
        if (maj_stat != GSS_S_COMPLETE) {
            if (display_file) {
                fprintf(display_file, "error in gss_display_status"
                        " called from <%s>\n", routine);
            }
            break;
        } else if (display_file)
            fprintf(display_file, "GSS-API error %s: %s\n", routine,
                (char *) msg.value);
        if (msg.length != 0)
            (void) gss_release_buffer(&min_stat, &msg);

        if (!msg_ctx)
            break;
    }
}

void display_status(char* routine, OM_uint32 maj_stat, OM_uint32 min_stat) {
    display_status_aux(routine, maj_stat, GSS_C_GSS_CODE);
    display_status_aux(routine, min_stat, GSS_C_MECH_CODE);
}
