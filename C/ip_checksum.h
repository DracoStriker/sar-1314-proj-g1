/* 
 * File:   ip_checksum.h
 * Author: simon
 *
 * Created on May 10, 2014, 11:27 PM
 */

#ifndef IP_CHECKSUM_H
#define	IP_CHECKSUM_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

    uint16_t ip_checksum(const void *buf, size_t hdr_len);

    uint16_t icmp_checksum(uint16_t *buffer, uint32_t size);

    uint16_t tcp_checksum(uint16_t *tcp_packet, int tcp_len, struct in_addr ip_src, struct in_addr ip_dst);

#ifdef	__cplusplus
}
#endif

#endif	/* IP_CHECKSUM_H */

