/* 
 * File:   c_global.h
 * Author: simon
 *
 * Created on June 1, 2014, 11:03 PM
 */

#ifndef C_GLOBAL_H
#define	C_GLOBAL_H

#ifdef	__cplusplus
extern "C" {
#endif

    extern unsigned char authentication;


#ifdef	__cplusplus
}
#endif

#endif	/* C_GLOBAL_H */

