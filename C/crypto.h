/* 
 * File:   crypto_hash.h
 * Author: Simão Reis <dracostriker@hotmail.com>
 *
 * Created on April 29, 2014, 6:36 PM
 */

#ifndef CRYPTO_H
#define	CRYPTO_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

    typedef void (*auth_algo)(uint8_t * /* plain_text */, size_t /* plain_text_len */, uint8_t * /* digest */, uint8_t * /* key */, size_t /* key_len */);

    auth_algo get_auth_algo(char * aalgo);

    unsigned int aalgo_key_length(char * aalgo);

    unsigned int aalgo_icv_length(char * aalgo);

#ifdef	__cplusplus
}
#endif

#endif	/* CRYPTO_H */

