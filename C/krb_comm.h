/* 
 * File:   krb_comm.h
 * Author: rafael
 *
 * Created on May 2, 2014, 12:20 AM
 */

#ifndef COMMON_H
#define	COMMON_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <sys/uio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <gssapi/gssapi.h>
#include <gssapi/gssapi_krb5.h>
#include <krb5.h>
#include <errno.h>
    

#define display_file stdout
#define g_mechOid GSS_C_NULL_OID

    typedef struct _ipsec_negociation {
        int32_t sid;
        char aalgo[17];
        char key[512];
        int32_t nonce;
    } ipsec_negociation;
    /* Esta api é baseada numa implementação exemplo fornecida pelo MIT.
     * O código é altamente influenciado por esta api e a sua implementação.
     */
    int ReadToken(int inSocket,
            gss_buffer_desc *token);

    int WriteToken(int inSocket,
            gss_buffer_desc token);

    int ReadEncryptedToken(int inSocket,
            const gss_ctx_id_t inContext,
            gss_buffer_t token);

    int WriteEncryptedToken(int inSocket,
            const gss_ctx_id_t inContext,
            gss_buffer_t token);

    void printError(int inError,
            const char *inString);

    void display_status(char *routine, OM_uint32 maj_stat, OM_uint32 min_stat);



#ifdef	__cplusplus
}
#endif

#endif	/* COMMONN_H */

