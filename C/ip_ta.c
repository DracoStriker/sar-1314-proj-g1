/* 
 * File:   ip_traffic_authorization.c
 * Author: Simão Reis <dracostriker@hotmail.com>
 *
 * Created on May 16, 2014, 3:36 PM
 */

#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <stdio.h>
#include <string.h>

#include "crypto.h"
#include "ip_ta.h"

#define ETHERNET_MTU 1500

/* internal functions*/

static int check_replay_window(IP_TA_CTX *ctx, uint32_t sqn);

/* traffic authorization context data base operations */

IP_TA_CTX_DB *build_ip_ta_ctx_db(uint32_t t_rows, char * aalgo, uint32_t session) {

    IP_TA_CTX_DB * ctx_db;
    if (t_rows < 1) {
        return NULL;
    }
    ctx_db = (IP_TA_CTX_DB *) calloc(1, sizeof (IP_TA_CTX_DB) + sizeof (IP_TA_CTX) * t_rows);
    ctx_db->last_sid = 0;
    ctx_db->t_rows = t_rows;
    ctx_db->n_rows = 0;
    ctx_db->session = session;
    strcpy(ctx_db->aalgo, aalgo);
    ctx_db->key_len = aalgo_key_length(aalgo);
    ctx_db->icv_len = aalgo_icv_length(aalgo);
    return ctx_db;
}

int ip_ta_ctx_db_insert(IP_TA_CTX_DB *ctx_db, char * key, uint32_t * sid) {
    uint32_t row;
    if (ctx_db->n_rows >= ctx_db->t_rows) {
        return CTX_DB_SID_OVERFLOW;
    }
    if (ctx_db->last_sid == (uint32_t) 0xffffffff) {
        return CTX_DB_MAX_SID;
    }
    for (row = 0; (row < ctx_db->n_rows) && (time(NULL) < ctx_db->entry[row].expiry); row++);
    // only if time expiry was not found a new entry is used
    if (row == ctx_db->n_rows) {
        ctx_db->n_rows++;
    }
    ctx_db->entry[row].sid = ++ctx_db->last_sid;
    memcpy(ctx_db->entry[row].key, key, ctx_db->key_len);
    ctx_db->entry[row].expiry = time(NULL) + ctx_db->session;
    ctx_db->entry[row].sqn = 32;
    ctx_db->entry[row].rwd = 0;
    *sid = ctx_db->last_sid;
    return 0;
}

int ip_ta_ctx_db_remove(IP_TA_CTX_DB *ctx_db, uint32_t sid) {
    uint32_t row;
    if (ctx_db->n_rows < 1) {
        return CTX_DB_SID_UNDERFLOW;
    }
    for (row = 0; (row < ctx_db->n_rows) && (ctx_db->entry[row].sid != sid); row++);
    if (ctx_db->entry[row].sid != sid) {
        return CTX_DB_SID_NOT_FOUND;
    }
    if (row != ctx_db->n_rows - 1) {
        ctx_db->entry[row] = ctx_db->entry[ctx_db->n_rows - 1];
    }
    ctx_db->n_rows--;
    return 0;
}

int ip_ta_ctx_db_lookup(IP_TA_CTX_DB *ctx_db, uint32_t sid, IP_TA_CTX ** ctx) {
    uint32_t row;
    if (ctx_db->n_rows < 1) {
        return CTX_DB_SID_UNDERFLOW;
    }
    for (row = 0; (row < ctx_db->n_rows) && (ctx_db->entry[row].sid != sid); row++);
    if (ctx_db->entry[row].sid != sid) {
        return CTX_DB_SID_NOT_FOUND;
    }
    if (time(NULL) > ctx_db->entry[row].expiry) {
        return CTX_DB_SESSION_EXPIRY;
    }
    *ctx = &(ctx_db->entry[row]);
    return 0;
}

void ip_ta_ctx_db_destroy(IP_TA_CTX_DB *ctx_db) {
    free(ctx_db);
}

/* traffic protection context data base operations */

IP_TP_CTX_DB *build_ip_tp_ctx_db(uint32_t t_rows) {
    IP_TP_CTX_DB * ctx_db;
    if (t_rows < 1) {
        return NULL;
    }
    ctx_db = (IP_TP_CTX_DB *) calloc(1, sizeof (IP_TP_CTX_DB) + sizeof (IP_TP_CTX) * t_rows);
    ctx_db->t_rows = t_rows;
    return ctx_db;
}

int ip_tp_ctx_db_insert(IP_TP_CTX_DB * ctx_db, uint32_t sid, struct in_addr addr, char * key, char * aalgo) {
    uint32_t row;
    if (ctx_db->n_rows >= ctx_db->t_rows) {
        return CTX_DB_SID_OVERFLOW;
    }
    for (row = 0; (row < ctx_db->n_rows) && (ctx_db->entry[row].addr.s_addr != addr.s_addr); row++);
    // if address was not found it's a new security association
    if (ctx_db->entry[row].addr.s_addr != addr.s_addr) {
        ctx_db->n_rows++;
    }
    ctx_db->entry[row].sid = sid;
    ctx_db->entry[row].sqn = 0;
    ctx_db->entry[row].addr = addr;
    ctx_db->entry[row].key_len = aalgo_key_length(aalgo);
    ctx_db->entry[row].icv_len = aalgo_icv_length(aalgo);
    memcpy(ctx_db->entry[row].key, key, ctx_db->entry[row].key_len);
    strcpy(ctx_db->entry[row].aalgo, aalgo);
    return 0;
}

int ip_tp_ctx_db_remove(IP_TP_CTX_DB * ctx_db, struct in_addr addr) {
    uint32_t row;
    if (ctx_db->n_rows < 1) {
        return CTX_DB_SID_UNDERFLOW;
    }
    for (row = 0; (row < ctx_db->n_rows) && (ctx_db->entry[row].addr.s_addr != addr.s_addr); row++);
    if (ctx_db->entry[row].addr.s_addr != addr.s_addr) {
        return CTX_DB_SID_NOT_FOUND;
    }
    if (row != ctx_db->n_rows - 1) {
        ctx_db->entry[row] = ctx_db->entry[ctx_db->n_rows - 1];
    }
    ctx_db->n_rows--;
    return 0;
}

int ip_tp_ctx_db_lookup(IP_TP_CTX_DB * ctx_db, struct in_addr addr, IP_TP_CTX ** ctx) {
    uint32_t row;
    if (ctx_db->n_rows < 1) {
        return CTX_DB_SID_UNDERFLOW;
    }
    for (row = 0; (row < ctx_db->n_rows) && (ctx_db->entry[row].addr.s_addr != addr.s_addr); row++);
    if (ctx_db->entry[row].addr.s_addr != addr.s_addr) {
        return CTX_DB_SID_NOT_FOUND;
    }
    *ctx = &(ctx_db->entry[row]);
    return 0;
}

void ip_tp_ctx_db_destroy(IP_TP_CTX_DB *ctx_db) {
    free(ctx_db);
}

/* packet operations */

void * protect_packet(void * packet, unsigned int packet_len, IP_TP_CTX_DB * ctx_db, unsigned int * ta_packet_len) {

    static uint8_t ta_packet[ETHERNET_MTU];
    IP_TP_CTX * ctx;
    int new_len;
    int status;
    int ip_hdr_len;

    struct ip * packet_ip_hdr = (struct ip *) packet;

    if ((status = ip_tp_ctx_db_lookup(ctx_db, packet_ip_hdr->ip_dst, &ctx)) < 0) {

        printf("Packet doesn't need authorization.\n");
        *ta_packet_len = packet_len;
        return packet;
    }
    
    ip_hdr_len = sizeof (struct ip) + sizeof (IP_AA_HDR) + ctx->icv_len;
    ip_hdr_len += ip_hdr_len % 4;

    if ((new_len = packet_len - sizeof(struct ip) + ip_hdr_len) > ETHERNET_MTU) {

        printf("Packet too large to add authorization footer.\n");
        *ta_packet_len = 0;
        return NULL;
    }

    printf("IP header backup.\n");
    struct ip * ta_packet_ip = (struct ip *) &ta_packet[0];
    struct in_addr ip_src;
    uint8_t ip_tos;
    uint8_t ip_ttl;
    uint16_t ip_off;
    uint16_t ip_id;

    *ta_packet_ip = *packet_ip_hdr;

    ip_src = ta_packet_ip->ip_src;
    ip_tos = ta_packet_ip->ip_tos;
    ip_ttl = ta_packet_ip->ip_ttl;
    ip_off = ta_packet_ip->ip_off;
    ip_id = ta_packet_ip->ip_id;

    ta_packet_ip->ip_src.s_addr = 0;
    ta_packet_ip->ip_tos = 0;
    ta_packet_ip->ip_ttl = 0;
    ta_packet_ip->ip_off = 0;
    ta_packet_ip->ip_sum = 0;
    ta_packet_ip->ip_id = 0;
    ta_packet_ip->ip_hl = ip_hdr_len / 4;

    IP_AA_HDR * aa_hdr = (IP_AA_HDR *) (ta_packet_ip + 1);

    aa_hdr->opts = 0x8A; // random options
    aa_hdr->len = sizeof (IP_AA_HDR) + ctx->icv_len;
    aa_hdr->unused = 0;
    
    printf("Calculating protected packet length.\n");
    ta_packet_ip->ip_len = htons(new_len);

    struct icmp *ta_packet_icmp;
    struct ih_idseq idseq;
    uint16_t cksum;

    struct udphdr *ta_packet_udp;
    uint16_t source;

    struct tcphdr *ta_packet_tcp;
    uint16_t window;

    printf("Copying packet.\n");
    memcpy(&ta_packet[ip_hdr_len], &((uint8_t *) packet)[sizeof (struct ip)], packet_len - sizeof (struct ip));

    switch (ta_packet_ip->ip_p) {

        case IPPROTO_ICMP:
            printf("ICMP header backup.\n");
            ta_packet_icmp = (struct icmp *) &ta_packet[ip_hdr_len];
            idseq = ta_packet_icmp->icmp_hun.ih_idseq;
            ta_packet_icmp->icmp_hun.ih_idseq.icd_id = 0;
            ta_packet_icmp->icmp_hun.ih_idseq.icd_seq = 0;
            cksum = ta_packet_icmp->icmp_cksum;
            ta_packet_icmp->icmp_cksum = 0;
            break;

        case IPPROTO_UDP:
            printf("UDP header backup.\n");
            ta_packet_udp = (struct udphdr *) &ta_packet[ip_hdr_len];
            source = ta_packet_udp->source;
            ta_packet_udp->source = 0;
            ta_packet_udp->check = 0;
            break;

        case IPPROTO_TCP:
            printf("TCP header backup.\n");
            ta_packet_tcp = (struct tcphdr *) &ta_packet[ip_hdr_len];
            source = ta_packet_tcp->source;
            ta_packet_tcp->source = 0;
            cksum = ta_packet_tcp->check;
            ta_packet_tcp->check = 0;
            window = ta_packet_tcp->window;
            ta_packet_tcp->window = 0;
            break;
    }

    printf("Getting sqn.\n");
    aa_hdr->sqn = htonl(ctx->sqn++);
    printf("Getting sid.\n");
    aa_hdr->sid = htonl(ctx->sid);

    printf("Reseting icv.\n");
    memset(&aa_hdr->icv, 0, ctx->icv_len);

    // Calculate ICV
    printf("Obtaining authentication algorithm.\n");
    auth_algo digest = get_auth_algo(ctx->aalgo);

    printf("Algorithm obtained at %p\n", digest);

    printf("Calculating ICV.\n");
    digest(&ta_packet[0], new_len, (uint8_t *) & aa_hdr->icv, ctx->key, ctx->key_len);

    int c;
    for (c = 0; c < ctx->icv_len; c++) {
        printf("%02x ", aa_hdr->icv[c]);
    }
    printf("\n");

    switch (ta_packet_ip->ip_p) {

        case IPPROTO_ICMP:
            printf("Recovering ICMP header.\n");
            ta_packet_icmp->icmp_hun.ih_idseq = idseq;
            ta_packet_icmp->icmp_cksum = cksum;
            break;

        case IPPROTO_UDP:
            printf("Recovering UDP header.\n");
            ta_packet_udp->source = source;
            break;

        case IPPROTO_TCP:
            printf("Recovering TCP header.\n");
            ta_packet_tcp->source = source;
            ta_packet_tcp->window = window;
            ta_packet_tcp->check = cksum;
            break;
    }

    printf("Recovering IP header.\n");
    ta_packet_ip->ip_src = ip_src;
    ta_packet_ip->ip_tos = ip_tos;
    ta_packet_ip->ip_ttl = ip_ttl;
    ta_packet_ip->ip_off = ip_off;
    ta_packet_ip->ip_id = ip_id;
    ta_packet_ip->ip_sum = ip_checksum((const void *) ta_packet, ip_hdr_len);

    *ta_packet_len = new_len;

    printf("Packet protected!\n");
    return &ta_packet[0];
}

int authorize_packet(void * ta_packet, unsigned int ta_packet_len, IP_TA_CTX_DB * ctx_db, unsigned int * packet_len) {

    IP_TA_CTX * ctx;
    int status;
    
    struct ip * ta_packet_ip = (struct ip *) &((uint8_t *) ta_packet)[0];
    
    int ip_hdr_len = ta_packet_ip->ip_hl * 4;
    
    if (ip_hdr_len != (sizeof(struct ip) + sizeof(IP_AA_HDR) + ctx_db->icv_len)) {
        printf("Packet without protection!\n");
        *packet_len = 0;
        return INVALID_PACKET_NOT_PROTECTED;
    }
    
    IP_AA_HDR * aa_hdr = (IP_AA_HDR *) (ta_packet_ip + 1);

    uint32_t sid = htonl(aa_hdr->sid);
    printf("Getting sid: %d\n", sid);

    uint32_t sqn = htonl(aa_hdr->sqn);
    printf("Getting sqn: %d\n", sqn);

    if ((status = ip_ta_ctx_db_lookup(ctx_db, sid, &ctx)) < 0) {

        printf("sid not found.\n");
        return INVALID_PACKET_SID_NOT_FOUND;
    }

    printf("Backup IP header.\n");
    struct in_addr ip_src;
    uint8_t ip_tos;
    uint8_t ip_ttl;
    uint16_t ip_off;
    uint16_t ip_id;

    ip_src = ta_packet_ip->ip_src;
    ip_tos = ta_packet_ip->ip_tos;
    ip_ttl = ta_packet_ip->ip_ttl;
    ip_off = ta_packet_ip->ip_off;
    ip_id = ta_packet_ip->ip_id;

    ta_packet_ip->ip_src.s_addr = 0;
    ta_packet_ip->ip_tos = 0;
    ta_packet_ip->ip_ttl = 0;
    ta_packet_ip->ip_off = 0;
    ta_packet_ip->ip_sum = 0;
    ta_packet_ip->ip_id = 0;

    struct icmp *ta_packet_icmp;
    struct ih_idseq idseq;
    uint16_t cksum;
    
    struct udphdr *ta_packet_udp;
    uint16_t source;

    struct tcphdr *ta_packet_tcp;
    uint16_t window;

    switch (ta_packet_ip->ip_p) {

        case IPPROTO_ICMP:
            printf("Backup ICMP header.\n");
            ta_packet_icmp = (struct icmp *) &((uint8_t *)ta_packet)[ip_hdr_len];
            idseq = ta_packet_icmp->icmp_hun.ih_idseq;
            ta_packet_icmp->icmp_hun.ih_idseq.icd_id = 0;
            ta_packet_icmp->icmp_hun.ih_idseq.icd_seq = 0;
            cksum = ta_packet_icmp->icmp_cksum;
            ta_packet_icmp->icmp_cksum = 0;
            break;

        case IPPROTO_UDP:
            printf("Backup UDP header.\n");
            ta_packet_udp = (struct udphdr *) &((uint8_t *)ta_packet)[ip_hdr_len];
            source = ta_packet_udp->source;
            ta_packet_udp->source = 0;
            ta_packet_udp->check = 0;
            break;

        case IPPROTO_TCP:
            printf("Backup TCP header.\n");
            ta_packet_tcp = (struct tcphdr *) &((uint8_t *)ta_packet)[ip_hdr_len];
            source = ta_packet_tcp->source;
            ta_packet_tcp->source = 0;
            cksum = ta_packet_tcp->check;
            ta_packet_tcp->check = 0;
            window = ta_packet_tcp->window;
            ta_packet_tcp->window = 0;
            break;
    }

    //Check ICV
    unsigned char * icv[20];
    printf("Backup ICV.\n");
    memcpy(icv, &aa_hdr->icv, ctx_db->icv_len);

    printf("ICV: ");
    int c;
    for (c = 0; c < ctx_db->icv_len; c++) {
        printf("%02x ", aa_hdr->icv[c]);
    }
    printf("\n");

    printf("Reset ICV.\n");
    memset(&aa_hdr->icv, 0, ctx_db->icv_len);
    printf("Getting authentication algorithm.\n");
    auth_algo digest = get_auth_algo(ctx_db->aalgo);
    printf("Calculating new ICV.\n");
    digest(&((uint8_t *) ta_packet)[0], ta_packet_len, (uint8_t *) & aa_hdr->icv, ctx->key, ctx_db->key_len);

    printf("Comparing new and old ICV.\n");
    if (memcmp(&aa_hdr->icv, icv, ctx_db->icv_len) != 0) {
        printf("Invalid ICV!\n");
        return INVALID_PACKET_INCORRECT_ICV;
    }
    
    printf("Valid ICV!\n");

    // Check replay window
    printf("Check replay window.\n");
    if (check_replay_window(ctx, sqn)) {

        printf("replay attack!\n");
        return INVALID_PACKET_REPLAY_ATTACK;
    }

    switch (ta_packet_ip->ip_p) {

        case IPPROTO_ICMP:
            printf("Recovering ICMP header.\n");
            ta_packet_icmp->icmp_hun.ih_idseq = idseq;
            ta_packet_icmp->icmp_cksum = cksum;
            break;

        case IPPROTO_UDP:
            printf("Recovering UDP header.\n");
            ta_packet_udp->source = source;
            break;

        case IPPROTO_TCP:
            printf("Recovering TCP header.\n");
            ta_packet_tcp->source = source;
            ta_packet_tcp->window = window;
            ta_packet_tcp->check = cksum;
            break;
    }

    printf("Recovering IP header.\n");
    ta_packet_ip->ip_src = ip_src;
    ta_packet_ip->ip_id = ip_id;
    ta_packet_ip->ip_tos = ip_tos;
    ta_packet_ip->ip_ttl = ip_ttl;
    ta_packet_ip->ip_off = ip_off;
    ta_packet_ip->ip_sum = ip_checksum((const void *) ta_packet, ip_hdr_len);

    *packet_len = ta_packet_len;
    return 0;
}

static int check_replay_window(IP_TA_CTX *ctx, uint32_t sqn) {
    uint32_t delta;
    int replay;
    if (sqn > ctx->sqn) {
        replay = 0;
        delta = sqn - ctx->sqn;
        ctx->rwd >>= delta;
        ctx->rwd |= 0x80000000;
        ctx->sqn = sqn;
    } else if ((sqn <= ctx->sqn) && (sqn >= (ctx->sqn - 31))) {
        delta = ctx->sqn - sqn;
        if ((ctx->rwd >> (31 - delta)) & 0x00000001) {
            replay = 1;
        } else {
            replay = 0;
            ctx->rwd |= (1 << (31 - delta));
        }
    } else {
        replay = 1;
    }
    return replay;
}