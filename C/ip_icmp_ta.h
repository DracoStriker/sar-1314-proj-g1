/* 
 * File:   ip_ta_icmp.h
 * Author: Simão Reis <dracostriker@hotmail.com>
 *
 * Created on April 29, 2014, 4:52 PM
 */

#ifndef IP_ICMP_TA_H
#define	IP_ICMP_TA_H

#ifdef	__cplusplus
extern "C" {
#endif

#define AUTH_REQUEST 0xAA
#define AUTH_REQUEST_PADDING_LENGTH (sizeof (struct ip) + 64 / 8)

    /*
     * Data structures.
     */
    struct auth_request {
        uint8_t padding[AUTH_REQUEST_PADDING_LENGTH];
        uint8_t magic_num;
        uint8_t code;
        uint8_t cs_auth;
        uint8_t cssos_auth;
        int port;
        struct in_addr ssos_addr;
        char service_name[33]; // max 32
        char realm[33]; // max 32
    };

#ifdef	__cplusplus
}
#endif

#endif	/* IP_ICMP_TA_H */

