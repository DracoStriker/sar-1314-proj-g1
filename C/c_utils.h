/* 
 * File:   c_utils.h
 * Author: Simão Reis <dracostriker@hotmail.com>
 *
 * Created on May 12, 2014, 8:16 PM
 */

#ifndef C_UTILS_H
#define	C_UTILS_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <netinet/ip.h>
    
#include "ip_ta.h"
#include "ip_icmp_ta.h"

    struct c_krb_params {
        IP_TP_CTX_DB * ctx_db;
        pthread_mutex_t * lock;
        struct in_addr ip_src;
        struct in_addr ip_dst;
        struct auth_request ar;
    };


#ifdef	__cplusplus
}
#endif

#endif	/* C_UTILS_H */

