/* 
 * File:   krb_c.h
 * Author: rafael
 *
 * Created on May 31, 2014, 9:02 PM
 */

#ifndef KRB_C_H
#define	KRB_C_H

#ifdef	__cplusplus
extern "C" {
#endif

void * run_client(void * args);

#ifdef	__cplusplus
}
#endif

#endif	/* KRB_C_H */

