/* 
 * File:   c.c
 * Author: Simão Reis <dracostriker@hotmail.com>
 *
 * Created on May 12, 2014, 5:53 PM
 */

#include <arpa/inet.h>
#include <net/if.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <linux/netfilter.h>	/* for NF_ACCEPT */
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libconfig.h>

#include "ip_icmp_ta.h"
#include "ip_checksum.h"
#include "c_utils.h"
#include "ip_ta.h"
#include "crypto.h"
#include "krb_c.h"
#include "c_global.h"
#include "color.h"
/**
 * Defines.
 */

#define NUM_QUEUE_THREADS 2

/*
 * Allusion to internal functions.
 */

static int read_config(void);

/*
 * Callback functions.
 */

int queue_in_cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data);
int queue_out_tp_cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data);

/*
 * Global variables.
 */

IP_TP_CTX_DB * ctx_db;
config_t cfg; /* Returns all parameters in this structure */
char *config_file_name = "c.config";

unsigned char authentication = 0;
pthread_mutex_t lock;

uint32_t n_sa;

nfq_callback *cb[NUM_QUEUE_THREADS] = {queue_in_cb, queue_out_tp_cb}; // NFQUEUE Callbacks

/**
 * 
 * @param args
 * @return 
 */
void *run_queue(void *args) {

    pthread_t thr_id = pthread_self();

    struct nfq_handle *h = (struct nfq_handle *) args;
    int fd = nfq_fd(h);

    char buf[4096] __attribute__((aligned));
    int rv;
    while ((rv = recv(fd, buf, sizeof (buf), 0)) && rv >= 0) {
        printf(ANSI_COLOR_GREEN "Thread '%lu' received packet." ANSI_COLOR_RESET "\n", (unsigned long) thr_id);
        nfq_handle_packet(h, buf, rv);
    }

    printf("Thread '%lu' closing library handle.\n", (unsigned long) thr_id);
    nfq_close(h);

    return (NULL);
}

/**
 * 
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char **argv) {

    pthread_t thr[NUM_QUEUE_THREADS];
    struct nfq_handle * h[NUM_QUEUE_THREADS];
    struct nfq_q_handle * qh[NUM_QUEUE_THREADS];
    void * ret = NULL;
    int status;
    int i;

    /*Initialization */
    config_init(&cfg);

    /* Read configuration file. */
    if ((status = read_config()) != 0) {
        return status;
    }

    /* Build the security association database. */
    if ((ctx_db = build_ip_tp_ctx_db(n_sa)) == NULL) {
        printf("Couldn't build the security database.\n");
        return -1;
    }

    //////////////////////////////////////////////////////////////
    /*
        struct in_addr addr = {.s_addr = inet_addr("192.168.1.27")};
        if (ip_tp_ctx_db_insert(ctx_db, 1, addr, "52067876183690269988", "hmac-sha1")) {
            printf("Insert failed!\n");
            return -1;
        }

        IP_TP_CTX * ctx;
        if ((status = ip_tp_ctx_db_lookup(ctx_db, addr, &ctx)) < 0) {
            printf("Lookup failed!\n");
            return -1;
        }
        printf("SID: %u\nSQN: %u\nDST: %s\nKEY_LEN: %d\nAALGO: %s\nICV_LEN: %d\nKEY: ", ctx->sid, ctx->sqn, inet_ntoa(ctx->addr), ctx->key_len, ctx->aalgo, ctx->icv_len);

        int c;
        for (c = 0; c < ctx->key_len; c++) {
            printf("%02x ", ctx->key[c]);
        }
        printf("\n");
     */
    //////////////////////////////////////////////////////////////

    /* init the nfqueues */

    for (i = 0; i < NUM_QUEUE_THREADS; i++) {

        printf("opening library handle\n");
        if (!(h[i] = nfq_open())) {
            fprintf(stderr, "error during nfq_open()\n");
            exit(EXIT_FAILURE);
        }

        printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
        if (nfq_unbind_pf(h[i], AF_INET) < 0) {
            fprintf(stderr, "error during nfq_unbind_pf()\n");
            exit(EXIT_FAILURE);
        }

        printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
        if (nfq_bind_pf(h[i], AF_INET) < 0) {
            fprintf(stderr, "error during nfq_bind_pf()\n");
            exit(EXIT_FAILURE);
        }

        printf("binding this socket to queue '%d'\n", i);
        if (!(qh[i] = nfq_create_queue(h[i], i, cb[i], NULL))) {
            fprintf(stderr, "error during nfq_create_queue()\n");
            exit(EXIT_FAILURE);
        }

        printf("setting copy_packet mode\n");
        if (nfq_set_mode(qh[i], NFQNL_COPY_PACKET, 0xffff) < 0) {
            fprintf(stderr, "can't set packet_copy mode\n");
            exit(EXIT_FAILURE);
        }

        /* start queue handling thread */
        if (pthread_create(&thr[i], NULL, run_queue, h[i])) {
            fprintf(stderr, "pthread_create() failed\n");
            exit(EXIT_FAILURE);
        }
    }

    /* wait for threads */
    for (i = 0; i < NUM_QUEUE_THREADS; i++) {
        pthread_join(thr[i], &ret);
    }

    ip_tp_ctx_db_destroy(ctx_db);
    config_destroy(&cfg);

    return (EXIT_SUCCESS);
}

/**
 * 
 * @param qh
 * @param nfmsg
 * @param nfa
 * @param data
 * @return 
 */
int queue_in_cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data) {

    struct nfqnl_msg_packet_hdr *ph = nfq_get_msg_packet_hdr(nfa);
    int id;
    if (ph) {
        id = ntohl(ph->packet_id);
    }

    struct ip *packet;
    nfq_get_payload(nfa, (unsigned char **) &packet);

    //printf("Protocol=%d\n", packet->ip_p);

    struct icmphdr *icmp = (struct icmphdr *) (packet + 1);
    struct auth_request *ar = (struct auth_request *) (icmp + 1);

    pthread_t thr_krb;

    unsigned char auth;

    pthread_mutex_lock(&lock);
    auth = authentication;
    //printf("authentication: %d\n",authentication);
    pthread_mutex_unlock(&lock);
    
    if (!auth) {
        
        pthread_mutex_lock(&lock);
        authentication = 1;
        pthread_mutex_unlock(&lock);

        /* Start kerberos auth thread */
        struct c_krb_params params = {.ctx_db = ctx_db, .lock = &lock, .ip_src = packet->ip_src, .ip_dst = packet->ip_dst, .ar = *ar};
        if (pthread_create(&thr_krb, NULL, run_client, &params)) {
            fprintf(stderr, "pthread_create() failed\n");
            exit(EXIT_FAILURE);
        }
        fprintf(stderr, "pthread_create()\n");
    }

    //printf("set verdict DROP\n");
    return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
}

/**
 * 
 * @param qh
 * @param nfmsg
 * @param nfa
 * @param data
 * @return 
 */
int queue_out_tp_cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data) {

    struct nfqnl_msg_packet_hdr *ph = nfq_get_msg_packet_hdr(nfa);
    int id;
    if (ph) {
        id = ntohl(ph->packet_id);
    }

    void *packet;
    int packet_len = nfq_get_payload(nfa, (unsigned char **) &packet);
    unsigned int ta_packet_len;

    void * ta_packet;
    if ((ta_packet = protect_packet(packet, packet_len, ctx_db, &ta_packet_len)) != NULL) {

        printf(ANSI_COLOR_BLUE "set Verdict ACCEPT" ANSI_COLOR_RESET "\n");
        return nfq_set_verdict(qh, id, NF_ACCEPT, ta_packet_len, (const unsigned char *) ta_packet);

    } else {

        printf(ANSI_COLOR_RED "set Verdict DROP" ANSI_COLOR_RESET "\n");
        return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
    }
}

static int read_config(void) {

    /* Read the file. If there is an error, report it and exit. */
    if (!config_read_file(&cfg, config_file_name)) {
        printf("No '%s' configuration file.\n", config_file_name);
        printf("%s:%d - %s\n", config_error_file(&cfg), config_error_line(&cfg), config_error_text(&cfg));
        config_destroy(&cfg);
        return -1;
    }

    /* Get the number of security associations */
    if (config_lookup_int(&cfg, "n_sa", &n_sa)) {
        printf("n_sa=%d;\n", n_sa);
    } else {
        printf("No 'n_sa' setting in configuration file.\n");
        return -1;
    }

    if (n_sa < 1) {
        printf("The must be at least one security association.");
        return -1;
    }

    return 0;
}