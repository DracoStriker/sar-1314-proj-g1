/*  LADO cliente parte SSOS

1 - Ligar ao servidor

2 - Iniciar security context. Enviar

3 - Autenticar-se,ou seja, prova que é o owner do TGT.Recebe token to servidor, verifica.

4 - Envia o pedido de token para o serviço S

5 - Recebe o token do serviço S(ticket)

6 - Verifica se o ticket é valido. Usa a chave pessoal para decifrar o token.

7 - Se for válido guarda o ticket Ts e session key Ks.


Convem referir que o ticket nao e nada mais do que o triplo(ID do cliente, endereço do cliente, chave de sessão) assinado com a chave do serviço


------------------------------------------------Lado cliente parte AAG------------------------------------------------

1 - Ligar ao servidor

2 - Iniciar contexto e envia-lo

Vamos ter que enviar o ticket e uma auth

3 - Autenticar-se assinando com a chave de sessão adquirida no SSOS

4 - Receber resposta do serviço. Ele vai verificar que o cliente é valido pelo ticket e pela autenticação

5 - Se o resultado é positivo pode ser iniciado o tunel ipsec e a comunicação segura. É usada a chave de sessão.

 */

#include <arpa/inet.h>

#include "krb_comm.h"
#include "c_utils.h"
#include "c_global.h"
#include "ip_ta.h"
#include "color.h"

#define KRBCONFOUT "/etc/krb.conf"
#define USERDATA "./tmp"

int myrealm = 0;
char realm[33];
char aag[17];
char ssos[17];
char serviceName[33];

/*Update do ficheiro de configuração krb5.conf  
 * export KRB5_CONFIG=${KRB5_CONFIG}/etc/mykrb5.conf
 */

static int createConfig() {
    
    static char command[] = "./krbconf.sh %s %s\n";
    static char conf[sizeof(command) + sizeof(realm) + sizeof(ssos)];
    snprintf(conf,sizeof(command) + strlen(realm) + strlen(ssos),command,realm,ssos);
    printf("REALM: %s\n",realm);
    printf("SSOS:  %s\n",ssos);
    printf("COMMAND: %s\n",conf);
    system(conf);
    setenv("KRB5_CONFIG", KRBCONFOUT, 1);
    return 0;
}

/*
 * Método responsável por criar a cache local ao cliente caso esta não exista. Ao cliente vai
 * ser pedida a password através de uma prompt.
 */

static int krb5_auth() {

    /* kerberos variable Initialization*/
    krb5_principal k5princ;
    krb5_error_code k5err;
    krb5_creds cred;
    krb5_ccache ccdef;
    krb5_context k5ctx;
    int error = 0;
    /*Prompt data retrieval*/
    system("./prompt.sh");
    errno = 0;
    FILE *fin;
    fin = fopen(USERDATA, "r");
    if (errno) {
        error = errno;
    }
    char ch = NULL;
    char hostname[50];
    char password[50];
    int i = 0;
    if (!error) {
        ch = (char) getc(fin);
        int index = 0;
        while (ch != EOF) {
            if (ch != '\n') {
                if (i == 0) {
                    hostname[index++] = ch;
                } else {
                    password[index++] = ch;
                }
            } else {
                if (i == 0) {
                    hostname[index] = '\0';
                    i++;
                } else {
                    password[index] = '\0';
                }
                index = 0;
            }
            ch = (char) getc(fin);
        }

        fclose(fin);
        remove(USERDATA);
    }
    printf("user: %s\npass: %s\n", hostname, password);
    if (!error) {
        /*Kinit*/
        k5err = krb5_init_context(&k5ctx);

        if (!k5err) {
            printf("oi\n");
            k5err = krb5_build_principal(k5ctx, &k5princ, strlen(realm), realm, hostname, NULL);
        }
        if (!k5err) {
            if (DEBUG) {
                printf("Getting TGT...\n");
            }
            k5err = krb5_get_init_creds_password(k5ctx, &cred, k5princ, password, NULL, NULL, 0, NULL, NULL);
        }

        if (!k5err) {
            if (DEBUG) {
                printf("Resolving cache...\n");
            }
            uid_t uid = getuid();
            char cache[25];
            snprintf(cache, sizeof (cache), "/tmp/krb5cc_%d", uid);
            k5err = krb5_cc_resolve(k5ctx, cache, &ccdef);

            if (DEBUG) {
                printf("Getting default cache...\n");
            }
        }

        if (!k5err) {
            k5err = krb5_cc_initialize(k5ctx, ccdef, k5princ);
        }

        if (!k5err) {
            if (DEBUG) {
                printf("Storing creds in cache...\n");
            }
            k5err = krb5_cc_store_cred(k5ctx, ccdef, &cred);
        }

        if (!k5princ) {
            krb5_free_principal(k5ctx, k5princ);
        }

        if (!k5err) {
            krb5_free_cred_contents(k5ctx, &cred);
        }
        if (!ccdef) {
            krb5_cc_close(k5ctx, ccdef);
        }

        if (!k5ctx) {
            krb5_free_context(k5ctx);
        }
    }

    if (!error) {
        error = k5err;
    }
    return error;
}

/*
 * Método responsável por ligar a um servidor remoto. São precisos três dados:
 * 
 * Input:
 * String hostname - Nome do principal do cliente.
 * 
 * Porta - Porta a ligar
 * 
 * Output:
 * outChannel - identificador do canal de comunicação 
 */
static int Connect(const char *inHost, int inPort, int *outChannel) {
    int error = 0;
    int sockfd = -1; //inicialização do número de canal
    struct hostent *host = NULL;
    struct sockaddr_in saddr;

    host = gethostbyname(inHost);
    if (host == NULL) {
        error = errno;
    }

    //Tendo obtido e preenchido a estrutura hostent. Vamos pegar nos dados que precisamos para preencher a estrutura sockaddr.
    if (!error) {
        bzero(&saddr, sizeof (saddr));
        saddr.sin_family = host->h_addrtype;
        memcpy((char *) &saddr.sin_addr, host->h_addr, sizeof (saddr.sin_addr));
        saddr.sin_port = htons(inPort);

        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0) {
            error = errno;
        }
    }

    //A estrutura sockaddr é usada para ligar ao servidor remoto.
    if (!error) {
        error = connect(sockfd, (struct sockaddr *) &saddr, sizeof (saddr));
        if (error < 0) {
            error = errno;
        }
    }

    // Verifica se deu erro. Se não deu anuncia que a ligação foi estabelecida e retorna o id do canal.
    if (!error) {
        if (DEBUG) {
            printf("connecting to host '%s' on port %d\n", inHost, inPort);
        }
        *outChannel = sockfd;
        sockfd = -1;
    } else {
        if (DEBUG) {
            printError(error, "Failed to open connection");
        }
    }

    if (sockfd >= 0) {
        close(sockfd);
    }

    return error;
}

/*
 * Método responsável pela autenticação do cliente. Pede o ticket e chave de sessão.
 */
static int Authenticate(int inChannel,
        const char *inServiceName,
        gss_ctx_id_t *outGSSContext) {
    int error = 0;
    OM_uint32 majorStatus;
    OM_uint32 minorStatus = 0;
    gss_name_t serviceName = NULL;
    gss_cred_id_t clientCredentials = GSS_C_BOTH; //credenciais inicialmente não existem. Obtidados na chamada gss_acquire_cred
    gss_ctx_id_t gssContext = GSS_C_NO_CONTEXT; //contexto a obter. inicialmente nao existe.
    OM_uint32 actualFlags = 0;

    gss_buffer_desc inputToken = {0, NULL}; /* buffer received from the server */
    gss_buffer_t inputTokenPtr = GSS_C_NO_BUFFER; /* ponteiro para o token recebido, inicialmente nao existe buffer*/

    if (inChannel < 0 || !inServiceName || !outGSSContext) {
        error = EINVAL;
    }
    if (!error) {
        //Obter as credenciais do utilizador
        if (!error) {
            majorStatus = gss_acquire_cred(&minorStatus, GSS_C_NO_NAME, GSS_C_INDEFINITE, GSS_C_NO_OID_SET,
                    GSS_C_INITIATE, &clientCredentials, NULL, NULL);

            printf("majorStatus: %d\n + minorStatus: %d\n", majorStatus, minorStatus);
            if (majorStatus != GSS_S_COMPLETE) {
                if (minorStatus == -1765328243) {
                    if (!error) {
                        error = krb5_auth();
                    }
                    majorStatus == GSS_S_CONTINUE_NEEDED;

                } else {
                    if (DEBUG) {
                        display_status("gss_acquire_cred", majorStatus, minorStatus);
                    }
                    error = minorStatus ? minorStatus : majorStatus;
                }
            }
        }
    }

    //obter o principal name do serviço
    if (!error) {
        gss_buffer_desc nameBuffer = {strlen(inServiceName), (char *) inServiceName};

        majorStatus = gss_import_name(&minorStatus, &nameBuffer, (gss_OID) GSS_KRB5_NT_PRINCIPAL_NAME, &serviceName);
        if (majorStatus != GSS_S_COMPLETE) {
            if (DEBUG) {
                display_status("gss_import_name(inServiceName)", majorStatus, minorStatus);
            }
            error = minorStatus ? minorStatus : majorStatus;
        }
    }

    majorStatus = GSS_S_CONTINUE_NEEDED;
    while (!error && (majorStatus != GSS_S_COMPLETE)) {

        gss_buffer_desc outputToken = {0, NULL}; /* buffer to send to the server */
        OM_uint32 requestedFlags = (GSS_C_MUTUAL_FLAG | GSS_C_REPLAY_FLAG | GSS_C_SEQUENCE_FLAG |
                GSS_C_CONF_FLAG | GSS_C_INTEG_FLAG);

        if (DEBUG) {
            printf("Calling gss_init_sec_context...\n");
        }

        majorStatus = gss_init_sec_context(&minorStatus,
                clientCredentials,
                &gssContext,
                serviceName,
                GSS_C_NULL_OID, /* mech_type: kerberos */
                requestedFlags,
                GSS_C_INDEFINITE,
                GSS_C_NO_CHANNEL_BINDINGS,
                inputTokenPtr,
                NULL /* actual_mech_type */,
                &outputToken,
                &actualFlags,
                NULL /* time_rec */);

        /* Send the output token to the server (even on error) */
        if ((outputToken.length > 0) && (outputToken.value != NULL)) {
            error = WriteToken(inChannel, outputToken);
            /* free the output token */
            gss_release_buffer(&minorStatus, &outputToken);
        }

        if (!error) {
            if (majorStatus == GSS_S_CONTINUE_NEEDED) {
                /* Protocol requires another packet exchange */
                /* Read another input token from the server */
                if (inputToken.value != NULL) {
                    //inputToken.length = 0;
                    inputToken.value = NULL; /* don't double-free */
                }
                gss_buffer_desc inputToken = {0, NULL};
                error = ReadToken(inChannel, &inputToken);
                inputTokenPtr = &inputToken;

            } else if (minorStatus == -1765328189) {
                if (!error) {
                    error = krb5_auth();
                }
                majorStatus == GSS_S_CONTINUE_NEEDED;

            } else if (majorStatus != GSS_S_COMPLETE) {
                if (DEBUG) {
                    printf("Erro na autenticação\n");
                    display_status("gss_init_sec_context", majorStatus, minorStatus);
                }
                error = minorStatus ? minorStatus : majorStatus;
            }
        }
    }

    if (!error) {
        *outGSSContext = gssContext;
        gssContext = NULL;
    } else {
        if (DEBUG) {
            printError(error, "AuthenticateToServer failed");
        }
    }

    if (serviceName) {
        gss_release_name(&minorStatus, &serviceName);
    }

    if (clientCredentials != GSS_C_NO_CREDENTIAL && clientCredentials != NULL) {
        //gss_release_cred(&minorStatus, &clientCredentials);
        clientCredentials == GSS_C_NO_CREDENTIAL;
    }

    if (gssContext != GSS_C_NO_CONTEXT) {
        gss_delete_sec_context(&minorStatus, &gssContext, GSS_C_NO_BUFFER);
    }

    return error;
}

int ipsec_config(int inChannel,
        gss_ctx_id_t GSSContext,
        IP_TP_CTX_DB * ctx_db,
        pthread_mutex_t * lock,
        struct in_addr aag_addr) {

    ipsec_negociation *input;
    int error = 0;
    gss_buffer_desc inputToken;

    if (!inChannel) {
        error = EINVAL;
    }
    if (!GSSContext) {
        error = EINVAL;
    }
    if (!error) {
        printf("Reading negociation...\n");
        error = ReadEncryptedToken(inChannel, GSSContext, &inputToken);
        input = (ipsec_negociation*) inputToken.value;
    }

   if (!error) {
        if (DEBUG) {
            printf("Ipsec_Negociation \n SPI: %d\n auth_alg: %s\n key: %s\n", input->sid, input->aalgo, input->key);
        }
        if (DEBUG) {
            printf("Server message: '%s'\n", (char *) inputToken.value);
        }
        pthread_mutex_lock(lock);
        error = ip_tp_ctx_db_insert(ctx_db, input->sid, aag_addr, input->key, input->aalgo);
        pthread_mutex_unlock(lock);
    }
    return error;
}

void * run_client(void * args) {

    struct c_krb_params * krb_args = (struct c_krb_params *) args;
    int error = 0;
    int channel = -1; // numero de identificação do canal(ligação tcp)
    int port = krb_args->ar.port;
    gss_ctx_id_t gssContext = GSS_C_NO_CONTEXT; //context vai ser posteriormente inicializado
    OM_uint32 minorStatus = 0;
    
    strcpy(realm,krb_args->ar.realm);
    strcpy(aag,inet_ntoa(krb_args->ip_src));
    strcpy(ssos,inet_ntoa(krb_args->ar.ssos_addr));
    strcpy(serviceName,krb_args->ar.service_name);
    
    error = createConfig();

    if (!error) {
        error = Connect(aag, port, &channel);
    }

    if (!error) {
        error = Authenticate(channel, serviceName, &gssContext);
    }

    if (!error) {
        if (DEBUG) {
            printf("Going to exchange ipsec negociation\n");
        }
        error = ipsec_config(channel, gssContext, krb_args->ctx_db, krb_args->lock, krb_args->ip_src);
        //error = test(channel, gssContext);
    }

    if (error) {
        printError(error, "Client failed");

    } else {
        if (DEBUG) {
            printf("O cliente foi autenticado!\n");
        }
    }

    if (channel >= 0) {
        printf("Closing socket.\n");
        close(channel);
    }
    if (gssContext != GSS_C_NO_CONTEXT) {
        gss_delete_sec_context(&minorStatus, &gssContext, GSS_C_NO_BUFFER);
    }

    usleep(5000000);
    pthread_mutex_lock(krb_args->lock);
    authentication = 0;
    pthread_mutex_unlock(krb_args->lock);

    return NULL;
}