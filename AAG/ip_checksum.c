/* 
 * File:   ip_checksum.h
 * Author: simon
 *
 * Created on May 10, 2014, 11:27 PM
 */

#include <netinet/in.h>
#include <string.h>

#include "ip_checksum.h"

#define ETHERNET_MTU 1500

struct pseudoTcpHeader {
    struct in_addr ip_src;
    struct in_addr ip_dst;
    uint8_t zero; //always zero
    uint8_t protocol; // = 6;//for tcp
    uint16_t tcp_len;
    uint8_t tcp_packet[0];
};

uint16_t ip_checksum(const void *buf, size_t hdr_len) {
    unsigned long sum = 0;
    const uint16_t *ip1;

    ip1 = buf;
    while (hdr_len > 1) {
        sum += *ip1++;
        if (sum & 0x80000000)
            sum = (sum & 0xFFFF) + (sum >> 16);
        hdr_len -= 2;
    }

    while (sum >> 16)
        sum = (sum & 0xFFFF) + (sum >> 16);

    return (~sum);
}

uint16_t icmp_checksum(uint16_t *buffer, uint32_t size) {
    unsigned long cksum = 0;
    while (size > 1) {
        cksum += *buffer++;
        size -= sizeof (unsigned short);
    }
    if (size) {
        cksum += *(unsigned char *) buffer;
    }
    cksum = (cksum >> 16) + (cksum & 0xffff);
    cksum += (cksum >> 16);
    return (uint16_t) (~cksum);
}

uint16_t tcp_checksum(uint16_t * tcp_packet, int tcp_len, struct in_addr ip_src, struct in_addr ip_dst) {

    uint8_t pseudo_buffer[ETHERNET_MTU];
    struct pseudoTcpHeader *pseudo_hdr = (struct pseudoTcpHeader *) &pseudo_buffer[0];
    pseudo_hdr->ip_dst = ip_dst;
    pseudo_hdr->ip_src = ip_src;
    pseudo_hdr->protocol = 6;
    pseudo_hdr->tcp_len = htons(tcp_len);
    pseudo_hdr->zero = 0;
    memcpy(&pseudo_hdr->tcp_packet, tcp_packet, tcp_len);

    uint16_t * buffer = (uint16_t *) & pseudo_buffer[0];
    int size = tcp_len + sizeof (struct pseudoTcpHeader);

    // algorithm

    unsigned long cksum = 0;
    while (size > 1) {
        cksum += *buffer++;
        size -= sizeof (uint16_t);
    }
    if (size)
        cksum += *(uint8_t*) buffer;

    cksum = (cksum >> 16) + (cksum & 0xffff);
    cksum += (cksum >> 16);
    return (uint16_t) (~cksum);
}

