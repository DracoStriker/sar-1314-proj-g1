/* 
 * File:   crypto.c
 * Author: Simão Reis <dracostriker@hotmail.com>
 *
 * Created on May 12, 2014, 8:16 PM
 */

#include <stdlib.h>

#include "crypto.h"
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/evp.h>

/** Internal functions. */

static void hmac_md5(uint8_t * plain_text, size_t plain_text_len, uint8_t * digest, uint8_t * key, size_t key_len);
static void hmac_sha1(uint8_t * plain_text, size_t plain_text_len, uint8_t * digest, uint8_t * key, size_t key_len);

auth_algo get_auth_algo(char * aalgo) {
    if (strcmp(aalgo, "hmac-md5") == 0) {
        return &hmac_md5;
    } else if (strcmp(aalgo, "hmac-sha1") == 0) {
        return &hmac_sha1;
    } else {
        return NULL;
    }
}

/**
 * 
 * @param algo
 * @return 
 */
unsigned int aalgo_key_length(char * aalgo) {
    if (strcmp(aalgo, "hmac-md5") == 0) {
        return 128 / 8;
    } else if (strcmp(aalgo, "hmac-sha1") == 0) {
        return 160 / 8;
    } else {
        return 0;
    }
}

/**
 * 
 * @param algo
 * @return 
 */
unsigned int aalgo_icv_length(char * aalgo) {
    if (strcmp(aalgo, "hmac-md5") == 0) {
        return MD5_DIGEST_LENGTH;
    } else if (strcmp(aalgo, "hmac-sha1") == 0) {
        return SHA_DIGEST_LENGTH;
    } else {
        return 0;
    }
}

static void hmac_md5(uint8_t * plain_text, size_t plain_text_len, uint8_t * digest, uint8_t * key, size_t key_len) {
    int digest_len = MD5_DIGEST_LENGTH;
    HMAC(EVP_md5(), key, key_len, plain_text, plain_text_len, digest, &digest_len);
}

static void hmac_sha1(uint8_t * plain_text, size_t plain_text_len, uint8_t * digest, uint8_t * key, size_t key_len) {
    int digest_len = SHA_DIGEST_LENGTH;
    HMAC(EVP_sha1(), key, key_len, plain_text, plain_text_len, digest, &digest_len);
}