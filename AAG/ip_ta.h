/* 
 * File:   ip_traffic_authorization.h
 * Author: Simão Reis <dracostriker@hotmail.com>
 *
 * Created on May 16, 2014, 3:36 PM
 */

#ifndef IP_TA_H
#define	IP_TA_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <netinet/in.h>

#define CTX_DB_SESSION_EXPIRY -1
#define INVALID_PACKET_SID_NOT_FOUND -2
#define INVALID_PACKET_REPLAY_ATTACK -3
#define INVALID_PACKET_INCORRECT_ICV -4
#define INVALID_PACKET_NOT_PROTECTED -5

#define CTX_DB_SID_OVERFLOW -5
#define CTX_DB_MAX_SID -6
#define CTX_DB_SID_UNDERFLOW -7
#define CTX_DB_SID_NOT_FOUND -8

    /* traffic protection context */

    typedef struct ip_tp_ctx {
        uint32_t sid; // security id
        uint32_t sqn; // seq number
        struct in_addr addr; // AAG address
        uint8_t key[512]; // security association key
        uint8_t key_len; // key length
        uint8_t aalgo[17]; // authentication algorithm
        uint8_t icv_len; // icv length
    } IP_TP_CTX;

    /* traffic protection context data base */

    typedef struct ip_tp_ctx_db {
        uint32_t t_rows; // total rows
        uint32_t n_rows; // number rows
        IP_TP_CTX entry[0];
    } IP_TP_CTX_DB;

    /* traffic authorization context */

    typedef struct ta_ctx {
        uint32_t sid; // security id
        uint32_t sqn; // seq number
        uint32_t rwd; // replay window
        uint8_t key[512]; // security association key
        time_t expiry; // expiry date
    } IP_TA_CTX;

    /* traffic authorization context data base */

    typedef struct ip_ta_ctx_db {
        uint32_t last_sid; // last given security id
        uint32_t t_rows; // total rows
        uint32_t n_rows; // number rows
        time_t session; // session time
        char aalgo[17]; // authentication algorithm
        uint8_t key_len; // key length
        uint8_t icv_len; // icv length
        IP_TA_CTX entry[0];
    } IP_TA_CTX_DB;

    /* traffic authorization footer */

    typedef struct ip_aa_hdr {
        uint8_t opts;
        uint8_t len;
        uint16_t unused;
        uint32_t sqn;
        uint32_t sid;
        uint8_t icv[0];
    } IP_AA_HDR;

    /* traffic authorization context data base operations */

    IP_TA_CTX_DB *build_ip_ta_ctx_db(uint32_t t_rows, char * aalgo, uint32_t session);

    int ip_ta_ctx_db_insert(IP_TA_CTX_DB *ctx_db, char * key, uint32_t * sid);

    int ip_ta_ctx_db_remove(IP_TA_CTX_DB *ctx_db, uint32_t sid);

    int ip_ta_ctx_db_lookup(IP_TA_CTX_DB *ctx_db, uint32_t sid, IP_TA_CTX ** ctx);

    void ip_ta_ctx_db_destroy(IP_TA_CTX_DB *ctx_db);

    /* traffic protection context data base operations */

    IP_TP_CTX_DB *build_ip_tp_ctx_db(uint32_t t_rows);

    int ip_tp_ctx_db_insert(IP_TP_CTX_DB * ctx_db, uint32_t sid, struct in_addr addr, char * key, char * aalgo);

    int ip_tp_ctx_db_remove(IP_TP_CTX_DB * ctx_db, struct in_addr addr);

    int ip_tp_ctx_db_lookup(IP_TP_CTX_DB * ctx_db, struct in_addr addr, IP_TP_CTX ** ctx);

    void ip_tp_ctx_db_destroy(IP_TP_CTX_DB *ctx_db);

    /* packet operations */

    void * protect_packet(void * packet, unsigned int packet_len, IP_TP_CTX_DB * ctx_db, unsigned int * ta_packet_len);

    int authorize_packet(void * ta_packet, unsigned int ta_packet_len, IP_TA_CTX_DB * ctx_db, unsigned int * packet_len);

#ifdef	__cplusplus
}
#endif

#endif	/* IP_TRAFFIC_AUTHORIZATION_H */

