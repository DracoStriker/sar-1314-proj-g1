/* 
 * File:   debug.h
 * Author: rafael
 *
 * Created on May 14, 2014, 7:15 PM
 */

#ifndef DEBUG_H
#define	DEBUG_H

#ifdef	__cplusplus
extern "C" {
#endif

#ifdef DEBUG
#undef DEBUG
#define DEBUG 1
#else
#define DEBUG 0
#endif



#ifdef	__cplusplus
}
#endif

#endif	/* DEBUG_H */

