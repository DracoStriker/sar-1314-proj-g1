/* 
 * File:   krb_aag.h
 * Author: rafael
 *
 * Created on May 31, 2014, 7:04 PM
 */

#ifndef KRB_AAG_H
#define	KRB_AAG_H

#ifdef	__cplusplus
extern "C" {
#endif


    void * krb_run(void * args);

#ifdef	__cplusplus
}
#endif

#endif	/* KRB_AAG_H */

