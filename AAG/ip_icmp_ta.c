/* 
 * File:   ip_ta_icmp.c
 * Author: Simão Reis <dracostriker@hotmail.com>
 *
 * Created on April 29, 2014, 4:52 PM
 */

#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <unistd.h> /* for close */
#include <stdio.h>
#include <string.h>

#include "ip_icmp_ta.h"

/**
 * 
 * @param buf
 * @param code
 * @param iface
 * @param ssos_addr
 * @param cs_auth
 * @param cssos_auth
 * @param service_name_len
 * @param service_name
 * @return 
 */
void fill_icmp_ar_data(const uint8_t *buf, size_t buf_len, uint8_t code,
        struct in_addr ssos_addr, uint8_t cs_auth, uint8_t cssos_auth,
        const char *service_name, int port, const char *realm) {

    struct ip *ip;
    struct icmphdr *icmp;
    struct auth_request *ar;

    bzero((void *) buf, buf_len);

    ip = (struct ip *) buf;
    icmp = (struct icmphdr *) (ip + 1);
    ar = (struct auth_request *) (icmp + 1);

    ip->ip_v = 4;
    ip->ip_hl = sizeof *ip >> 2;
    ip->ip_tos = 0;
    ip->ip_len = htons(buf_len);
    ip->ip_id = htons(0);
    ip->ip_off = htons(0);
    ip->ip_ttl = 64;
    ip->ip_p = 1;
    ip->ip_sum = 0; /* Let kernel fills in */

    icmp->type = 3;
    icmp->code = 13;
    
    icmp->un.frag.__unused = htons(AUTH_REQUEST_PADDING_LENGTH / 4);

    ar->magic_num = AUTH_REQUEST;
    ar->code = code;
    ar->ssos_addr = ssos_addr;
    ar->cs_auth = cs_auth;
    ar->cssos_auth = cssos_auth;
    ar->ssos_addr = ssos_addr;
    ar->port = port;
    strcpy(ar->service_name, service_name);
    strcpy(ar->realm, realm);
}

/**
 * 
 * @param buf
 * @param packet
 */
void fill_icmp_ar_padding(const uint8_t *buf, const uint8_t *packet) {

    struct ip *ip;
    struct icmphdr *icmp;
    struct auth_request *ar;

    ip = (struct ip *) buf;
    icmp = (struct icmphdr *) (ip + 1);
    ar = (struct auth_request *) (icmp + 1);

    memcpy(&ar->padding, packet, AUTH_REQUEST_PADDING_LENGTH);
}
