/* 
 * File:   color.h
 * Author: Simão Reis <dracostriker@hotmail.com>
 *
 * Created on May 31, 2014, 3:06 PM
 */

#ifndef COLOR_H
#define	COLOR_H

#ifdef	__cplusplus
extern "C" {
#endif

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#ifdef	__cplusplus
}
#endif

#endif	/* COLOR_H */

