
/* Server LADO SSOS
    
1 - setup listening socket

2 - accepting connections

//num ambiente stateful seria preciso criar uma nova thread para atender cada cliente
??3 - create new thread to deal with connection?

4 - fica à espera de receber o security context. chama o metodo accept_security_context e elabora uma resposta

5 - Depois de o utilizador estar autenticado e ter o devido TGT. O servidor recebe o serviço para o qual o utilizador quer aceder

6 - Verifica se o serviço é valido

7 - Envia a resposta. Com o ticket para o serviço e devida chave de sessão

------------------------------------------------Server Lado AAG------------------------------------------------
    
1 - setup listening socket

2 - accepting connections

//num ambiente stateful seria preciso criar uma nova thread para atender cada cliente
??3 - create new thread to deal with connection?

4 - Recebe o ticket. Faz o unwrap do ticket e verifica a identidade do cliente. Com a chave de sessão existente no ticket decifra
o auth enviado pelo cliente e compara a identidade.

5 - Se a identidade se verificar utiliza a chave de sessão para iniciar uma comunicação com ipsec com o cliente

 */

#include "krb_comm.h"
#include "aag_utils.h"
#include <pthread.h>

static int SetupListeningSocket(int inPort,
        int *outChannel) {
    int error = 0;
    int listenfd = -1;
    if (!outChannel) {
        error = EINVAL;
    }

    if (!error) {
        listenfd = socket(AF_INET, SOCK_STREAM, 0);
        if (listenfd < 0) {
            perror("creating socket");
            error = errno;
        }
    }
    //fill server address struct
    if (!error) {
        struct sockaddr_storage addressStorage;
        struct sockaddr_in *servaddr = (struct sockaddr_in *) &addressStorage;

        bzero(servaddr, sizeof (struct sockaddr_in));
        servaddr->sin_family = AF_INET;
        servaddr->sin_addr.s_addr = htonl(INADDR_ANY);
        servaddr->sin_port = htons(inPort);
        //bind the socket
        error = bind(listenfd, (struct sockaddr *) servaddr, sizeof (struct sockaddr_in));
        if (listenfd < 0) {
            error = errno;
        }
    }

    if (!error) {
        error = listen(listenfd, 5);
        if (error < 0) {
            error = errno;
        }
    }

    if (!error) {
        printf("listening on port %d\n", inPort);
        *outChannel = listenfd;
        listenfd = -1;
    } else {
        printError(error, "SetupListeningSocket failed");
    }

    if (listenfd >= 0) {
        close(listenfd);
    }

    return error;
}

static int Authenticate(int inSocket,
        gss_ctx_id_t *outGSSContext) {
    int error = 0;
    OM_uint32 majorStatus;
    OM_uint32 minorStatus = 0;
    gss_ctx_id_t gssContext = GSS_C_NO_CONTEXT;

    gss_buffer_desc inputToken; /* buffer received from the server */

    if (inSocket < 0) {
        error = EINVAL;
    }
    if (!outGSSContext) {
        error = EINVAL;
    }

    majorStatus = GSS_S_CONTINUE_NEEDED;
    while (!error && (majorStatus != GSS_S_COMPLETE)) {
        //Ler token enviado pelo cliente
        if (inputToken.value != NULL) {
            inputToken.value = NULL; /* don't double-free */
            inputToken.length = 0;
        }
        error = ReadToken(inSocket, &inputToken);
        //inputTokenPtr = &inputToken;
        if (!error) {
            /* buffer to send to the server */
            gss_buffer_desc outputToken = {0, NULL};
            if (DEBUG) {
                printf("Calling gss_accept_sec_context...\n");
            }
            majorStatus = gss_accept_sec_context(&minorStatus,
                    &gssContext,
                    GSS_C_NO_CREDENTIAL,
                    &inputToken,
                    GSS_C_NO_CHANNEL_BINDINGS,
                    NULL /* client_name */,
                    NULL /* actual_mech_type */,
                    &outputToken,
                    NULL /* req_flags */,
                    NULL /* time_rec */,
                    NULL /* delegated_cred_handle */);

            if ((outputToken.length > 0) && (outputToken.value != NULL)) {
                /* Send the output token to the client (even on error) */
                error = WriteToken(inSocket, outputToken);
                /* free the output token */
                gss_release_buffer(&minorStatus, &outputToken);
            }
        }
        if ((majorStatus != GSS_S_COMPLETE) && (majorStatus != GSS_S_CONTINUE_NEEDED)) {

            if (DEBUG) {
                display_status("gss_accept_sec_context", majorStatus, minorStatus);
            }
            error = minorStatus ? minorStatus : majorStatus;
        }
    }

    if (!error) {
        *outGSSContext = gssContext;
        gssContext = NULL;
    } else {
        if (DEBUG) {
            printError(error, "Authenticate failed");
        }
    }

    if (gssContext != GSS_C_NO_CONTEXT) {
        gss_delete_sec_context(&minorStatus, &gssContext, GSS_C_NO_BUFFER);
    }

    return error;
}

//Este método verifica que o serviço é de facto o pedido pelo aag.

static int Verify(gss_ctx_id_t inContext, char * service) {

    int error = 0; /*Codigo de erro*/
    OM_uint32 majorStatus; /*Retorno de erro principal*/
    OM_uint32 minorStatus = 0; /*Retorno de erro secundario*/
    gss_name_t serviceName = NULL; /*Nome do serviço*/
    char *servicePrincipal = NULL; /*Principal do serviço*/
    krb5_context context = NULL; /*Contexto kerberos*/
    krb5_principal principal = NULL; /*Principal kerberos*/

    if (!inContext) {
        error = EINVAL;
    }

    if (!error) {
        if (DEBUG) {
            printf("Obtaining service\n");
        }
        majorStatus = gss_inquire_context(&minorStatus,
                inContext,
                NULL,
                &serviceName,
                NULL, NULL, NULL, NULL, NULL);
        if (majorStatus != GSS_S_COMPLETE) {
            //error toma o valor do erro secundario(relativo ao mecanismo, no nosso caso o kerberos).
            //se nao existir toma o valor do erro principal.
            error = minorStatus ? minorStatus : majorStatus;
        }
    }

    if (!error) {
        if (DEBUG) {
            printf("display name\n");
        }
        /* Obter o principal do serviço apartir do nome obtido anteriormente */
        gss_buffer_desc nameToken;

        majorStatus = gss_display_name(&minorStatus,
                serviceName,
                &nameToken,
                NULL);
        if (majorStatus != GSS_S_COMPLETE) {
            error = minorStatus ? minorStatus : majorStatus;
        }

        if (!error) {
            servicePrincipal = malloc(nameToken.length + 1);
            if (servicePrincipal == NULL) {
                error = ENOMEM;
            }
        }

        if (!error) {
            memcpy(servicePrincipal, nameToken.value, nameToken.length);
            servicePrincipal[nameToken.length] = '\0';
        }

        if (nameToken.value) {
            gss_release_buffer(&minorStatus, &nameToken);
        }
    }

    if (!error) {
        if (!servicePrincipal) {
            error = EINVAL;
        }

        if (!error) {
            error = krb5_init_context(&context);
        }

        if (!error) {
            error = krb5_parse_name(context, servicePrincipal, &principal);
        }

        if (!error) {
            if (DEBUG) {
                printf("%s - %s\n", service, principal->data->data);
            }
            if (service && strcmp(service,
                    krb5_princ_name(context, principal)->data) != 0) {
                error = KRB5KRB_AP_WRONG_PRINC;
            }
        }

        if (principal) {
            krb5_free_principal(context, principal);
        }
        if (context) {
            krb5_free_context(context);
        }
    }
    if (serviceName) {
        gss_release_name(&minorStatus, &serviceName);
    }
    if (servicePrincipal) {
        free(servicePrincipal);
    }

    return error;
}

int ipsec_config(int channel,
        gss_ctx_id_t inContext,
        IP_TA_CTX_DB * ctx_db,
        pthread_mutex_t * lock) {

    ipsec_negociation * output;

    int error = 0;
    gss_buffer_desc outputToken;
    if (!channel) {
        error = EINVAL;
    }
    if (!inContext) {
        error = EINVAL;
    }

    if (!error) {
        //Criar a negociação ipsec e inserir a entrada na base de dados
        int sid;
        memcpy(&output->key, urandom(ctx_db->key_len), ctx_db->key_len);
        printf("Creating security association.\n");
        pthread_mutex_lock(lock);
        error = ip_ta_ctx_db_insert(ctx_db, output->key, &sid);
        pthread_mutex_unlock(lock);
        printf("SID: %d.\n", sid);
        printf("Preparing token.\n");
        
        //Valores da negociação ipsec  
        output->sid = sid;
        //memcpy(&output->nonce, urandom(sizeof (int32_t)), sizeof (int32_t));
        strcpy(output->aalgo, ctx_db->aalgo);
        
        //Copia da negociação para o buffer do Token Gss
        
        outputToken.value = (void *)output;
        outputToken.length = sizeof(ipsec_negociation);
        if (DEBUG) {
            //printf("Server to client Message: '%s' com tamanho: %d\n", (char *) outputToken.value, (int) outputToken.length);
        }
        printf("Write token.\n");
        if (!error) {
            error = WriteEncryptedToken(channel, inContext, &outputToken);
        }
    }
    if (error) {
            pthread_mutex_lock(lock);
            error = ip_ta_ctx_db_remove(ctx_db, output->sid);
            pthread_mutex_unlock(lock);    
    }
    return error;
}

void * run(void *arg) {

    struct aag_krb_params * krb_args = (struct aag_krb_params *) arg;
    int error = 0;
    OM_uint32 minorStatus;
    int channel = krb_args->channel;
    gss_ctx_id_t gssContext = GSS_C_NO_CONTEXT;

    //enquanto não ocorrer nenhum erro o servidor vai atender clientes
    error = Authenticate(channel, &gssContext);

    if (!error) {
        char * ptr = strchr(krb_args->service,'/');
        char service[33];
        strncpy(service,krb_args->service,strlen(krb_args->service) - strlen(ptr));
        error = Verify(gssContext,service);
    }

    //Negociação ipsec
    if (!error) {
        error = ipsec_config(channel, gssContext, krb_args->ctx_db, krb_args->lock);
        if (DEBUG) {
            printf("error: %d", error);
        }
    }

    if (channel >= 0) {
        close(channel);
    }

    if (gssContext != GSS_C_NO_CONTEXT) {
        gss_delete_sec_context(&minorStatus, &gssContext, GSS_C_NO_BUFFER);
    }

    if (error) {
        if (DEBUG) {
            printError(error, "Client authentication error");
        }
    } else {
        printf("Client negociation ended sucessfully!\n");
    }
}

void * krb_run(void * args) {

    struct aag_krb_params * krb_args = (struct aag_krb_params *) args;
    int error = 0;
    int listenChannel = -1;
    int channel = -1;
    int port = krb_args->port;
    pthread_t id;

    printf("Starting up...\n");

    //Servidor vai ficar à escuta
    error = SetupListeningSocket(port, &listenChannel);

    if (!error) {
        while (1) {
            if (listenChannel > 0) {
                channel = accept(listenChannel, NULL, NULL);
                krb_args->channel = channel;
                error = pthread_create(&id, NULL, &run, args);
                //printf("aag Main error = %d\n", error);
            } else {
                error = errno;
                printError(error, "Listenchannel error\n");
            }
        }
    }

    if (listenChannel >= 0) {
        close(listenChannel);
    }

    if (error) {
        printError(error, "Server could not begin! Shutting down..");
    }

    return 0;
}