/* 
 * File:   aag.c
 * Author: Simão Reis <dracostriker@hotmail.com>
 *
 * Created on April 25, 2014, 3:45 AM
 */

#include <arpa/inet.h>
#include <net/if.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <linux/netfilter.h>	/* for NF_ACCEPT */
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <libconfig.h> 

#include "ip_icmp_ta.h"
#include "ip_ta.h"
#include "ip_checksum.h"
#include "aag_utils.h"
#include "color.h"
#include "krb_aag.h"

/*
 * Defines.
 */

#define NUM_QUEUE_THREADS 1
#define NUM_RAW_SOCKETS 1

/**
 * Allusion to internal functions.
 */

static int read_config(void);

/*
 * Callback functions.
 */

int queue_in_cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data);

/*
 * Global variables
 */

IP_TA_CTX_DB * ctx_db;
pthread_mutex_t lock;

config_t cfg; /* Returns all parameters in this structure */
char *config_file_name = "aag.config";

uint8_t *buf; // authentication request with code 0
size_t buf_len;
/*
uint8_t *buf1; // authentication request with code 1
size_t buf1_len;
 */

uint32_t n_sa;
char *aalgo;
time_t session;
char *service;
char *realm;
struct in_addr ssos_addr;
int cs_auth;
int cssos_auth;
int port;

int s;

/*
 * Thread's mains.
 */

/**
 * 
 * @param args
 * @return 
 */
void *run_queue(void *args) {

    pthread_t thr_id = pthread_self();

    struct nfq_handle *h = (struct nfq_handle *) args;
    int fd = nfq_fd(h);

    char buf[4096] __attribute__((aligned));
    int rv;
    while ((rv = recv(fd, buf, sizeof (buf), 0)) && rv >= 0) {
        printf("Thread '%lu' received packet.\n", (unsigned long) thr_id);
        nfq_handle_packet(h, buf, rv);
    }

    printf("Thread '%lu' closing library handle.\n", (unsigned long) thr_id);
    nfq_close(h);

    return (NULL);
}

/**
 * 
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char **argv) {

    pthread_t thr;
    struct nfq_handle * h;
    struct nfq_q_handle * qh;
    pthread_t thr_kerb;
    void * ret = NULL;
    int on;
    int status;
    int i;

    /*Initialization */
    config_init(&cfg);

    /* Read configuration file. */
    if ((status = read_config()) != 0) {
        return status;
    }

    /* Build the security association database. */
    if ((ctx_db = build_ip_ta_ctx_db(n_sa, aalgo, session)) == NULL) {
        printf("Couldn't build the security database.\n");
        return -1;
    }

    /* initializing mutex */
    if (pthread_mutex_init(&lock, NULL) != 0) {
        printf("Mutex initialization failed.\n");
        return 1;
    }

    //////////////////////////////////////////////////////////////
    /*
        uint32_t sid;
        if (ip_ta_ctx_db_insert(ctx_db, "52067876183690269988", &sid)) {
            printf("Insert failed!\n");
            return -1;
        }

        IP_TA_CTX * ctx;
        if ((status = ip_ta_ctx_db_lookup(ctx_db, sid, &ctx)) < 0) {
            printf("Lookup failed! Error: %d\n", status);
            return -1;
        }
        printf("SID: %u\nSQN: %u\nWINDOW: %d\nEXPIRY: %s\nKEY: ", ctx->sid, ctx->sqn, ctx->rwd, ctime(&ctx->expiry));

        int c;
        for (c = 0; c < ctx_db->key_len; c++) {
            printf("%02x ", ctx->key[c]);
        }
        printf("\n");
     */
    //////////////////////////////////////////////////////////////

    /* fill the imutable fields of Authentication Request Messages */

    printf("init ICMP Authentication Error Message code=0\n");
    buf_len = sizeof (struct ip) + sizeof (struct icmphdr) + sizeof (struct auth_request);
    buf = (uint8_t *) malloc(buf_len);
    fill_icmp_ar_data(buf, buf_len, 0, ssos_addr, cs_auth, cssos_auth, service, port, realm);

    /* Open raw sockets */

    on = 1;

    /* Create RAW socket */
    if ((s = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) < 0) {
        perror("socket() error");
        /* If something wrong, just exit */
        return (EXIT_FAILURE);
    }

    /* socket options, tell the kernel we provide the IP structure */
    if (setsockopt(s, IPPROTO_IP, IP_HDRINCL, &on, sizeof (int)) < 0) {
        perror("setsockopt() for IP_HDRINCL error");
        return (EXIT_FAILURE);
    }

    /* init the nfqueues */

    printf("opening library handle\n");
    if (!(h = nfq_open())) {
        fprintf(stderr, "error during nfq_open()\n");
        exit(EXIT_FAILURE);
    }

    printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
    if (nfq_unbind_pf(h, AF_INET) < 0) {
        fprintf(stderr, "error during nfq_unbind_pf()\n");
        exit(EXIT_FAILURE);
    }

    printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
    if (nfq_bind_pf(h, AF_INET) < 0) {
        fprintf(stderr, "error during nfq_bind_pf()\n");
        exit(EXIT_FAILURE);
    }

    printf("binding this socket to queue '0'\n");
    if (!(qh = nfq_create_queue(h, 0, &queue_in_cb, NULL))) {
        fprintf(stderr, "error during nfq_create_queue()\n");
        exit(EXIT_FAILURE);
    }

    printf("setting copy_packet mode\n");
    if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
        fprintf(stderr, "can't set packet_copy mode\n");
        exit(EXIT_FAILURE);
    }

    /* start queue handling thread */
    if (pthread_create(&thr, NULL, run_queue, h)) {
        fprintf(stderr, "pthread_create() failed\n");
        exit(EXIT_FAILURE);
    }

    /* Start kerberos auth thread */
    struct aag_krb_params params = {.port = port, .ctx_db = ctx_db, .lock = &lock, .service = service};
    if (pthread_create(&thr_kerb, NULL, krb_run, &params)) {
        fprintf(stderr, "pthread_create() failed\n");
        exit(EXIT_FAILURE);
    }

    /* wait for threads */
    pthread_join(thr, &ret);
    pthread_join(thr_kerb, &ret);

    /* close sockets */
    close(s);

    pthread_mutex_destroy(&lock);
    ip_ta_ctx_db_destroy(ctx_db);
    config_destroy(&cfg);

    return (EXIT_SUCCESS);
}

/**
 * 
 * @param qh
 * @param nfmsg
 * @param nfa
 * @param data
 * @return 
 */
int queue_in_cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data) {

    struct nfqnl_msg_packet_hdr *ph = nfq_get_msg_packet_hdr(nfa);
    int id;
    if (ph) {
        id = ntohl(ph->packet_id);
    }

    struct ip *packet;
    int packet_len = nfq_get_payload(nfa, (unsigned char **) &packet);

    printf("Protocol=%d\n", packet->ip_p);

    int new_packet_len;
    struct sockaddr_in dst;

    pthread_mutex_lock(&lock);
    switch (authorize_packet(packet, packet_len, ctx_db, &new_packet_len)) {
        case 0:
            printf("PACKET LENGTH: %d\n", packet_len);
            printf("NEW PACKET LENGTH: %d\n", new_packet_len);

            pthread_mutex_unlock(&lock);

            printf("set verdict ACCEPT\n");
            return nfq_set_verdict(qh, id, NF_ACCEPT, new_packet_len, (const char *) packet);

        case INVALID_PACKET_NOT_PROTECTED:
        case INVALID_PACKET_SID_NOT_FOUND:
            dst = (struct sockaddr_in){.sin_addr = packet->ip_src, .sin_family = AF_INET};

            struct ip *ip = (struct ip *) &buf[0];

            ip->ip_src = packet->ip_dst;
            ip->ip_dst = packet->ip_src;
            fill_icmp_ar_padding(buf, (const uint8_t *) packet);

            struct icmphdr *icmp = (struct icmphdr *) (ip + 1);

            icmp->checksum = 0;
            icmp->checksum = icmp_checksum((void *) icmp, buf_len - sizeof (struct ip));

            if (sendto(s, buf, buf_len, 0, (struct sockaddr *) &dst, sizeof (dst)) < 0) {
                printf(ANSI_COLOR_RED "couldn't send ICMP Authentication Request code=0 to %s\n" ANSI_COLOR_RESET, inet_ntoa(dst.sin_addr));
            } else {
                printf(ANSI_COLOR_BLUE "sending ICMP Authentication Request code=0 to %s\n" ANSI_COLOR_RESET, inet_ntoa(dst.sin_addr));
            }

            pthread_mutex_unlock(&lock);

            printf("set verdict DROP\n");
            return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);

        case INVALID_PACKET_REPLAY_ATTACK:
        case INVALID_PACKET_INCORRECT_ICV:

            pthread_mutex_unlock(&lock);

            printf("set verdict DROP\n");
            return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
    }
}

static int read_config(void) {

    /* Read the file. If there is an error, report it and exit. */
    if (!config_read_file(&cfg, config_file_name)) {
        printf("No '%s' configuration file.\n", config_file_name);
        printf("%s:%d - %s\n", config_error_file(&cfg), config_error_line(&cfg), config_error_text(&cfg));
        config_destroy(&cfg);
        return -1;
    }

    /* Get the service name. */
    if (config_lookup_string(&cfg, "service", (const char **) &service)) {
        printf("service=\"%s\";\n", service);
    } else {
        printf("No 'service' setting in configuration file.\n");
        return -1;
    }

    if (strlen(service) < 1) {
        printf("Invalid 'service' setting in configuration file.\n");
        return -1;
    }

    /* Get the number of security associations */
    if (config_lookup_int(&cfg, "n_sa", &n_sa)) {
        printf("n_sa=%d;\n", n_sa);
    } else {
        printf("No 'n_sa' setting in configuration file.\n");
        return -1;
    }

    if (n_sa < 1) {
        printf("The must be at least one security association.");
        return -1;
    }

    /* Get the session period */
    if (config_lookup_int(&cfg, "session", (int *) &session)) {
        printf("session=%g;\n", difftime(session, 0));
    } else {
        printf("No 'session' setting in configuration file.\n");
        return -1;
    }

    if (session < 60) {
        printf("The security association period must be at least one minute.");
        return -1;
    }

    /* Get the authentication algorithm. */
    if (config_lookup_string(&cfg, "aalgo", (const char **) &aalgo)) {
        printf("aalgo=\"%s\";\n", aalgo);
    } else {
        printf("No 'aalgo' setting in configuration file.\n");
        return -1;
    }

    if ((strcmp(aalgo, "hmac-md5") != 0) && (strcmp(aalgo, "hmac-sha1") != 0)) {
        printf("Invalid 'aalgo' setting in configuration file.\n");
        return -1;
    }

    char *ssos;

    /* Get the SSOS address. */
    if (config_lookup_string(&cfg, "ssos", (const char **) &ssos)) {
        printf("ssos=\"%s\";\n", ssos);
    } else {
        printf("No 'ssos' setting in configuration file.\n");
        return -1;
    }

    ssos_addr.s_addr = inet_addr(ssos);
    if (ssos_addr.s_addr == (in_addr_t) (-1)) {
        printf("Invalid 'ssos' setting in configuration file.\n");
        return -1;
    }

    /* Get the C-S Authentication mode. */
    if (config_lookup_int(&cfg, "cs_auth", &cs_auth)) {
        printf("cs_auth=%d;\n", cs_auth);
    } else {
        printf("No 'cs_auth' setting in configuration file.\n");
        return -1;
    }

    /* Get the C-SSOS Authentication mode. */
    if (config_lookup_int(&cfg, "cssos_auth", &cssos_auth)) {
        printf("cssos_auth=%d;\n", cssos_auth);
    } else {
        printf("No 'cssos_auth' setting in configuration file.\n");
        return -1;
    }

    /* Get the kerberos port number. */
    if (config_lookup_int(&cfg, "port", &port)) {
        printf("port=%d;\n", port);
    } else {
        printf("No 'port' setting in configuration file.\n");
        return -1;
    }

    if ((unsigned int) port > (unsigned int) 0xFFFF) {
        printf("Invalid 'port' setting in configuration file.\n");
        return -1;
    }

    /* Get the kerberos realm. */
    if (config_lookup_string(&cfg, "realm", (const char **) &realm)) {
        printf("realm=\"%s\";\n", realm);
    } else {
        printf("No 'realm' setting in configuration file.\n");
        return -1;
    }

    if (strlen(realm) < 1) {
        printf("Invalid 'realm' setting in configuration file.\n");
        return -1;
    }

    return 0;
}
