
#include <stdio.h>
#include <stdlib.h>

#include "aag_utils.h"

/**
 * 
 * @param key_length
 * @return 
 */
unsigned char *urandom(unsigned int key_length) {

    FILE *f;
    static unsigned char key[512];

    /*open file */
    if ((f = fopen("/dev/urandom", "r")) == NULL) {
        exit(1);
    }

    printf("Generated Key: ");

    int i;
    for (i = 0; i < key_length; i++) {
        if ((key[i] = fgetc(f)) == EOF) {
            return NULL;
        }
        printf("%02x ", key[i]);
    }

    printf("\n");

    return &key[0];
}
