4th Year Course - Advanced Network Security
-------------------------------------------

Project Members
----------------
Simão Reis 

Rafael Saraiva

Project Description
-------------------

Usually traffic is authorized by firewalls based on its type, origin, destination,etc., but not on the identity of the user responsible for its generation.

The goal of this project was to explore this new way of authorize traffic targe-ting a particular organizational server or network.  The server/network guardian is able to authorize traffic based on the identity of the person that generated  that  traffic.   For  this  goal,  traffic  will  carry  user-related  authenticationtags,  which  will  be  added  on  a  needed  basis  and  with  minimum  intervention from users.  On the other hand, to minimize authentication operations required to people, a Single Sign-On (SSO) mechanism was explored to allow many traffic flows to be authenticated upon a single user authentication.

In this project was considered 4 types of core entities:  the client host (C), the server host (S), the guardian that authenticates and authorizes traffic to flow through from a client to a protected server/network (AAG), and a central Single Sign-On server (SSOS).

![alt text](architecture.png)

At host C run several applications, and one of them may initiate a communication with a server at host S. However, this communication is evaluated by an AAG, which decides upon its suitability. In our case, we used a policy in AAG that prevents any interaction with S to occur without the authenticationof C’s traffic.

The AAG, typically a packet filter firewall, will on a first phase either accept or reject a communication from a host to S (that it protects) based upon the effective  authentication  of  the  arriving  traffic.   In  other  words,  if  C’s  traffic carries  a  proper  authentication  tag  to  interact  with  S,  then  the  firewall  may consider the possibility to allow that communication to occur (based on other filtering strategies), otherwise it refuses the communication with a specific error message.

Once C gets a error message from an AAG, which should be distinct from all other error messages used in the Internet, C will follow the instruction providedin the error message, which may include:

1.  The type of traffic authentication that must be used in order to enable the C-S interaction;
2.  The SSOS that must be used to authenticate the client to the AAG.
3.  The type of authentication that should be performed between C and SSOS.

Using this information, the user of C will get authentication credentials from SSOS, will use them to setup the required traffic authentication protocol with the AAG and, once this done, should be able to communicate with S.

Implementation
--------------
* SSL Tunnels.
* SSOS with Kerberos.
* AAG based on the iptables packet filter.
* Error messages with ICMP datagram with new types.
* Processing  of  error  messages  in  host  C  using  again  the iptables packet  filter,  complemented  by  an  application  for  processing  the  new  error messages.